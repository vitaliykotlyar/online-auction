/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 04.01.16
 */

//myFunctions = require('./service'),
var express = require('express'),
    app = express(),
    path = require('path'),
    port = 8081,
    fs = require('fs'),
    rootProject = global.appRoot, //TODO найти лучшее решение
    bodyParser = require('body-parser'),
    dirPath = rootProject + '/server/data';
/**
 *
 * INIT MYSQL
 */
var mysql = require('mysql');
var client = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: '121',
    database: 'online_lots'
});


app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));



/**
 * static files
 */
app.get('/', function (req, res) {
    res.sendFile(path.join(rootProject + '/index.html'));
});

app.get('/static/*', function (req, res) {
    res.sendFile(path.join(rootProject + req.url));
});

/**
 * GET LOTS
 */

app.get('/lots', function (req, res) {

    console.log("GET LOTS");

    client.query('SELECT * FROM lots', function (error, result, fields) {
        if (result) {
            res.statusCode = 200;
            res.send(result);
        }
        else {
            res.statusCode = 400;
            res.send("error");
        }
    });


});

/**
 * GET DISCOUNTS
 */

app.get('/discounts', function (req, res) {

    console.log("GET DISCOUNTS");

    client.query('SELECT * FROM lots', function (error, result, fields) {
        if (result) {
            var tmpObj = [];
            result.map(function (elem) {
                if (elem.discount) {
                    tmpObj.push(elem);
                }
            });
            res.statusCode = 200;
            res.send(tmpObj);
        }
        else {
            res.statusCode = 400;
            res.send("error");
        }
    });


});


/**
 * ADD LOT
 */


app.post('/lot/add', function (req, res) {

    console.log("ADD LOT");

    var lot = {
        title: req.body.title || "empty title",
        description: req.body.description || "empty description",
        img_src: req.body.img_src || "http://www.comprasparaguai.com.br/media/fotos/modelos" +
        "/notebook_apple_macbook_pro_md101_intel_core_i5_25_ghz" +
        "_memoria_4gb_hd_500gb_133_18291_550x550.jpg",
        price: req.body.price || 0,
        start_price: req.body.price || 0,
        date_create_lot: new Date(),
        date_last_bid:new Date()
    };

    client.query('INSERT INTO lots SET ?', lot, function (err, result) {
        if (err) throw err;
        if (result) {
            res.statusCode = 200;
            res.send({'id': result.insertId});
        }
        else {
            res.statusCode = 400;
            res.send("error");
        }
    });
});

/**
 * UPDATE BID
 */


app.post('/bid/add', function (req, res) {

    console.log("UPDATE BID");

    if (parseInt(req.body.current_price) < parseInt(req.body.price)
        || !req.body.current_price) {

        var lot = {
            price: req.body.price,
            date_last_bid:new Date()
        }

        client.query(
            'UPDATE lots SET ? Where ID = ' + req.body.id,
            lot,
            function (err, result) {
                if (err) throw err;
                res.statusCode = 200;
                res.send({'status': 'ok'});
            }
        );
    } else {
        res.statusCode = 400;
        res.send("bid less than current price");
    }

});

/**
 * UPDATE lot
 */


app.post('/lot/update', function (req, res) {

    console.log("UPDATE lot");


        var lot = {
            auction: req.body.auction,
            id: req.body.id,
            price: req.body.price
        };

    client.query(
            'UPDATE lots SET ? Where ID = ' + req.body.id,
            lot,
            function (err, result) {
                if (err) throw err;
                res.statusCode = 200;
                res.send({'status': 'ok'});
            }
        );


});


app.listen(port);


console.log("server running " + port);
console.log("http://localhost:" + port);