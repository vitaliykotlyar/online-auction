/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 08.01.16
 */

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function changeHashOnValue(value) {
    window.location.hash = value;
};

var sortLots = {
    sortByPriceCheap: function (lots) {

        var arrTmp = clone(lots);

        function compare(a, b) {
            if (a.start_price < b.start_price)
                return -1;
            else if (a.start_price > b.start_price)
                return 1;
            else
                return 0;
        }

        arrTmp.sort(compare);
        return arrTmp;
    },
    sortByPriceExpansive: function (lots) {

        var arrTmp = clone(lots);

        function compare(a, b) {
            if (a.start_price > b.start_price)
                return -1;
            else if (a.start_price < b.start_price)
                return 1;
            else
                return 0;
        }

        arrTmp.sort(compare);
        return arrTmp;
    },
    sortByDateEarly: function (lots) {

        var arrTmp = clone(lots);

        arrTmp.sort(function (a, b) {
            return new Date(b.date_create_lot) - new Date(a.date_create_lot);
        });
        return arrTmp;
    },
    sortByDateLately: function (lots) {

        var arrTmp = clone(lots);

        arrTmp.sort(function (b, a) {
            return new Date(b.date_create_lot) - new Date(a.date_create_lot);
        });
        return arrTmp;
    }
}

function lotsSortBy(token,lots) {
    var sortedLots;
    switch (token) {
        case 'cheap':
            sortedLots = sortLots.sortByPriceCheap(lots);
            break;
        case 'expansive':
            sortedLots = sortLots.sortByPriceExpansive(lots);
            break;
        case 'early':
            sortedLots = sortLots.sortByDateEarly(lots);
            break;
        case 'lately':
            sortedLots = sortLots.sortByDateLately(lots);
            break;
    }
    return sortedLots;
}

/**
 *
 * ADDITIONAL FUNCTION
 */

function clone(obj) {
    if (obj == null || typeof(obj) != 'object')
        return obj;
    var temp = new obj.constructor();
    for (var key in obj)
        temp[key] = clone(obj[key]);
    return temp;
}

function changeFormatDate(date) {
    if (!date) return;
    var resultArr = date.match(/(\d\d\d\d-\d\d-\d\d)T(\d\d:\d\d)/);
    if (resultArr)
        return (resultArr[1] + " " + resultArr[2]).split('-').join('.');
    else return date;
}

function combineFormatDate(data) {
    data.date_create_lot = changeFormatDate(data.date_create_lot);
    data.date_last_bid = changeFormatDate(data.date_last_bid);
};

/**
 * SEARCH
 */

(function () {

    function Search() {
        this.resultSearch = [];
        this.searchByToken = function(token){
            var resultArray = [];
            common.api.lots.map(function (elem) {
                console.log("i");
                if(elem.title.indexOf(token) + 1) {
                    resultArray.push(elem);
                }
            });
            this.resultSearch = resultArray;
            console.log(resultArray);
        };
        this.getResultSearch = function () {
            return this.resultSearch;
        }
    };

    common.search = new Search();

})();
