/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 04.01.16
 */

$(document).ready(function () {

    if (window.history && window.history.pushState) {
        //if click button back reload content
        //The 'popstate' event only works when you push something before
        $(window).on('popstate', function () {
            common.spa.renderPage();
        });
    }

    var $body = $('body');

    $body.on("submit", '.js-add-lot-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);

        common.api.addLot($this);
        $('.modal').modal('hide');
        common.spa.renderPage();
    });

    $body.on("submit", '.js-add-bid-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);

        common.api.addBid($this);
        $('.modal').modal('hide');
        common.spa.renderPage();
    });
    /**
     * SEARCH
     */

    $body.on("submit", '.js-form-search', function (e) {
        e.preventDefault();
        var $this = $(this);

        if ($this.serializeObject().token != "") {
            common.search.searchByToken($this.serializeObject().token);
            changeHashOnValue('!/search');
        }
        common.spa.renderPage();
    });


    /**
     * ACTIONS WITH LOT
     */

    $body.on('click', '.js-to-auction', function (e) {
        var $this = $(this);
        common.api.toAuction($this.data('id'));
        common.spa.renderPage();
    });

    $body.on('click', '.js-remove-from-auction', function (e) {
        var $this = $(this);
        common.api.removeFromAuction($this.data('id'));
        common.spa.renderPage();
    });

    $body.on('click', '.js-add-lot', function (e) {
        var $modalAddLot = $('.js-modal-add-lot');
        $modalAddLot.modal('toggle');
    })

    $body.on('change', '.js-sort-lot', function (e) {
        common.lotsFilOrSort = lotsSortBy(this.value, common.lotsFilOrSort);
        common.spa.renderLots(common.lotsFilOrSort);
        //$('.js-sort-lot').val(this.value);
    });

    /**
     * FILTERS
     */

    $body.on('click', '.js-title-filter', function () {
        var $this = $(this);
        $this.toggleClass('off')
            .closest('.js-filter').find('.js-filter-inner')
            .animate({height: 'toggle'}, 350);
    });

    $body.on('submit', '.js-form-filters', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);

        var inputData = $this.serializeObject();

        common.lotsFilOrSort = common.filters.filterByPrice(common.lotsOnPage,
            parseInt(inputData.priceFrom),
            parseInt(inputData.priceTo));

        common.spa.renderLots(common.lotsFilOrSort);
    });

    $body.on('click', '.js-clear-filters', function () {
        common.filters.clearAllFilter();
    });

    /**
     * FOOTER
     */
    $body.on('click', '.js-open-contact', function () {
        var $mapContacts = $('.map-contacts');
        if ($mapContacts.hasClass('min-version'))
            $mapContacts.removeClass('min-version');
    });
    $body.on('click', '.js-close', function (e) {
        console.log("in cloxe");
        e.stopPropagation();
        var $mapContacts = $('.map-contacts');
        if (!$mapContacts.hasClass('min-version'))
            $mapContacts.addClass('min-version');
    });
    /**
     * PAGINATION
     */
    $body.on('click', '.js-on-pagination', function (e) {
        var $this = $(this);
        $this.closest('.js-pagination').find('.tool-pagination').removeClass('active');
        $this.addClass('active');
        common.pagination.onPageByNumber($this.data('number'));
    });

    /**
     * MENU
     */
    $body.on('click', '.js-toggle-menu', function (e) {
        var $this = $(this);
        $this.toggleClass('off')
            .closest('.js-wr-menu').find('.js-menu')
            .animate({height: 'toggle'}, 350);
    });
});