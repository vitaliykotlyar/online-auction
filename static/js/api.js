/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 09.01.16
 */
//because api.js run first
var common = {
    templates: [],
    lots: [],
    title: {
        lots: "We have what you're looking for.",
        discount: 'We have some discounts special for you.'
    }
};


(function () {

    function Api() {
        this.getFromServer = function () {
            var dataResponse;
            $.ajax({
                url: '/lots',
                method: 'get',
                cache: false,
                async: false
            }).done(function (data) {
                dataResponse = data;
            }).error(function () {
                console.log("error");
            });
            return dataResponse;
        };
        this.lots = this.getFromServer();
        this.get = function () {
            return this.lots;
        };
        this.put = function (lots) {
            this.lots = lots;
        };
        this.getLotById = function (id) {
            for (var i = 0; i < this.lots.length; i++) {
                if (this.lots[i].id == id) {
                    return this.lots[i];
                }
            }
        };
        this.getLotsFilterByProperty = function (property) {
            var tmpObj = [];
            this.lots.map(function (elem) {
                if (elem[property]) {
                    tmpObj.push(elem);
                }
            });
            return tmpObj;
        };
        this.addLot = function ($form) {
            var objData = $form.serializeObject();
            var lot = {
                title: objData.title || "empty title",
                description: objData.description || "empty description",
                img_src: objData.img_src || "http://www.comprasparaguai.com.br/media/fotos/modelos" +
                "/notebook_apple_macbook_pro_md101_intel_core_i5_25_ghz" +
                "_memoria_4gb_hd_500gb_133_18291_550x550.jpg",
                price: objData.price || 0,
                start_price: objData.price || 0,
                date_create_lot: new Date(),
                date_last_bid: new Date()
            };
            this.lots.push(lot);
            common.lotsFilOrSort.push(clone(lot));
            common.lotsOnPage.push(clone(lot));

            this.sendToServer(
                $form.serialize(), '/lot/add', 'post',
                function () {
                    notification.create("Lot success added", 'success');
                },
                function (data) {
                    notification.create(data.responseText, 'error');
                });
        };
        this.addBid = function ($form) {
            var objData = $form.serializeObject();
            if (parseInt(objData.current_price) < parseInt(objData.price)
                || !objData.current_price) {

                var lot = this.getLotById(objData.id);

                lot.price = objData.price;
                lot.date_last_bid = (new Date()).toISOString();

                this.sendToServer(
                    $form.serialize(), '/bid/add', 'post',
                    function () {
                        notification.create("Bid success added", 'success');
                    },
                    function (data) {
                        notification.create(data.responseText, 'error');
                    });
            }
            else {
                notification.create("bid less than current price", 'error');
            }
        };
        this.toAuction = function (id) {
            console.log("in fucn");
            var lot = this.getLotById(id);
            lot.auction = true;
            lot.price = lot.start_price;

            this.sendToServer(
                {
                    auction: true,
                    id: lot.id,
                    price: 0
                },
                '/lot/update',
                'post',
                function () {
                    notification.create("Lot success added to auction", 'success');
                },
                function (data) {
                    notification.create(data.responseText, 'error');
                }
            )

        };
        this.removeFromAuction = function (id) {
            var lot = this.getLotById(id);
            lot.auction = null;

            this.sendToServer(
                {
                    auction: null,
                    id: lot.id,
                    price: lot.price
                },
                '/lot/update',
                'post',
                function () {
                    notification.create("Lot success removed from auction", 'success');
                },
                function (data) {
                    notification.create(data.responseText, 'error');
                }
            )
        };
        this.sendToServer = function (data, url, method, success, fail) {
            $.ajax({
                url: url,
                method: method,
                dataType: 'json',
                //contentType: 'application/json; charset=utf-8',
                data: data,
                cache: false
            }).success(function (data) {
                console.log("in done");
                (typeof success == "function") ? success(data) : false;
            }).error(function (data) {
                (typeof fail == "function") ? fail(data) : false;
                console.log("error");
            });
        }
    };

    common.api = new Api();

})();
