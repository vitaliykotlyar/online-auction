/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 10.01.16
 */


(function () {
    function Pagination() {
        this.lotsForPages = [];
        this.splitInputArr = function () {
            //TODO переделать через сплит
            var j = 0;
            var inputArr = common.lotsFilOrSort;
            this.lotsForPages[j] = [];
            this.lotsForPages[j].push(inputArr[0]);

            for (var i = 1; i < inputArr.length; i++) {
                if (i % 10 == 0) {
                    j++;
                    this.lotsForPages[j] = [];
                }
                this.lotsForPages[j].push(inputArr[i]);
            }
        };
        this.init = function () {
            this.lotsForPages = [];
            if (!common.lotsFilOrSort ||common.lotsFilOrSort.length <= 11)
                return false;
            this.splitInputArr();

            this.addButton();
            this.onPageByNumber(0);
        };
        this.onPageByNumber = function (number) {
            common.lotsOnPage = clone(this.lotsForPages[number]);
            common.lotsFilOrSort = clone(this.lotsForPages[number]);
            common.spa.renderLots(common.lotsFilOrSort);
        };
        this.addButton = function () {
            var $container = $('.js-pagination');
            var data = [];
            for (var i = 0; i < this.lotsForPages.length; i++) {
                data.push({name: i + 1, number: i});
            }
            $container.append(common.templates["js-template-pagination"]({data: data}));
            $container.find('.tool-pagination').first().addClass('active');
        }
    };

    common.pagination = new Pagination();

})();