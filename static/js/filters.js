/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 10.01.16
 */

(function () {

    function Filters() {
        this.filterByPrice = function (lots, priceFrom, priceTo) {
            var flag;
            var flag1;
            if (!priceTo) flag = true;
            if (!priceFrom) flag1 = true;
            var resultArr = [];
            lots.map(function (elem) {
                if ((elem.start_price >= priceFrom || flag1 )
                    && (elem.start_price <= priceTo || flag))
                    resultArr.push(elem);
            });


            return resultArr;
        };
        this.clearAllFilter = function () {
            //common.lotsOnPage = lotsSortBy('cheap', common.api.get());
            //common.spa.renderLots();
            common.spa.renderPage();
        }
    };

    common.filters = new Filters();

})();
