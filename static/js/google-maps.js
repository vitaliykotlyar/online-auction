/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 10.01.16
 */

(function () {
    function initialize() {
        var position = {lat: 50.430452, lng: 30.487239};
        var mapCanvas = document.getElementById('map');
        var mapOptions = {
            center: new google.maps.LatLng(position.lat - 0.003, position.lng),
            zoom: 15,
            disableDefaultUI: true,
            zoomControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
        };
        var map = new google.maps.Map(mapCanvas, mapOptions)
        var marker = new google.maps.Marker({
            position: position,
            map: map,
            title: 'iLots'
        });
        var url = window.location.hostname;
    }

    google.maps.event.addDomListener(window, 'load', initialize);
})();

