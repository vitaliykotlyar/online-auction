/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 09.01.16
 */

(function () {

    function Spa() {
        this.hash = {};
        this.hash.get = function () {
            var hashTmp = window.location.hash.substr(3);
            var arrTmp = [];
            return arrTmp.concat(hashTmp, hashTmp.split('/'));
        };


        this.renderPage = function () {
            var hash = this.hash.get();
            //hash[0] - all, hash[1] - page, hash[2] - param

            if (!hash[1]) {
                hash[1] = "main";
            }

            var template = common.templates["js-template-page-" + hash[1]];
            var templateWithData = {};

            if (template) {
                templateWithData = this.getTemplateWithData(template, hash);
                changeHashOnValue("#!/" + hash[0]);
            }
            else {
                templateWithData = common.templates["js-template-page-not-found"];
            }
            common.$content.empty().append(templateWithData);

            common.pagination.init();
        };
        this.renderLots = function (lots) {
            var data = lots || common.lotsOnPage;
            var valueSort = $('.js-sort-lot').val();
            data = lotsSortBy(valueSort, data);

            var $wrLots = $('.js-wr-lots');
            $wrLots.empty().append(common.templates["js-template-lots"]({lots: data}));
        };
        this.getTemplateWithData = function (template, hash) {
            var data = {};
            switch (hash[1]) {
                case 'lots':
                    var title = common.title.lots;
                    var lots;

                    if (/^lots\/\w*$/.test(hash[0])) { //if lots with parameter
                        lots = common.api.getLotsFilterByProperty(hash[2]);
                        if (hash[2] == 'discount')
                            title = common.title.discount;

                    }
                    else {
                        lots = common.api.get();
                    }
                    data.title = title;
                    //сортируем по дате
                    data.lots = lotsSortBy('early', lots);
                    //два одинаковых объекта нужно при фильтрации
                    common.lotsOnPage = data.lots; //for sorting and filtering lots
                    common.lotsFilOrSort = clone(data.lots); //for sorting and filtering lots
                    break;
                case 'lot':
                    var lot = common.api.getLotById(hash[2]);
                    combineFormatDate(lot);
                    data = {lot: lot};
                    break;
                case 'search':
                    data = {lots: common.search.getResultSearch()};
                    console.log(data);
                    break;
                default:
                    data = {};
            }

            return template(data);
        };
    };

    common.spa = new Spa();

})();

