/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 09.01.16
 */
//because api.js run first
var common = {
    templates: [],
    lots: [],
    title: {
        lots: "We have what you're looking for.",
        discount: 'We have some discounts special for you.'
    }
};


(function () {

    function Api() {
        this.getFromServer = function () {
            var dataResponse;
            $.ajax({
                url: '/lots',
                method: 'get',
                cache: false,
                async: false
            }).done(function (data) {
                dataResponse = data;
            }).error(function () {
                console.log("error");
            });
            return dataResponse;
        };
        this.lots = this.getFromServer();
        this.get = function () {
            return this.lots;
        };
        this.put = function (lots) {
            this.lots = lots;
        };
        this.getLotById = function (id) {
            for (var i = 0; i < this.lots.length; i++) {
                if (this.lots[i].id == id) {
                    return this.lots[i];
                }
            }
        };
        this.getLotsFilterByProperty = function (property) {
            var tmpObj = [];
            this.lots.map(function (elem) {
                if (elem[property]) {
                    tmpObj.push(elem);
                }
            });
            return tmpObj;
        };
        this.addLot = function ($form) {
            var objData = $form.serializeObject();
            var lot = {
                title: objData.title || "empty title",
                description: objData.description || "empty description",
                img_src: objData.img_src || "http://www.comprasparaguai.com.br/media/fotos/modelos" +
                "/notebook_apple_macbook_pro_md101_intel_core_i5_25_ghz" +
                "_memoria_4gb_hd_500gb_133_18291_550x550.jpg",
                price: objData.price || 0,
                start_price: objData.price || 0,
                date_create_lot: new Date(),
                date_last_bid: new Date()
            };
            this.lots.push(lot);
            common.lotsFilOrSort.push(clone(lot));
            common.lotsOnPage.push(clone(lot));

            this.sendToServer(
                $form.serialize(), '/lot/add', 'post',
                function () {
                    notification.create("Lot success added", 'success');
                },
                function (data) {
                    notification.create(data.responseText, 'error');
                });
        };
        this.addBid = function ($form) {
            var objData = $form.serializeObject();
            if (parseInt(objData.current_price) < parseInt(objData.price)
                || !objData.current_price) {

                var lot = this.getLotById(objData.id);

                lot.price = objData.price;
                lot.date_last_bid = (new Date()).toISOString();

                this.sendToServer(
                    $form.serialize(), '/bid/add', 'post',
                    function () {
                        notification.create("Bid success added", 'success');
                    },
                    function (data) {
                        notification.create(data.responseText, 'error');
                    });
            }
            else {
                notification.create("bid less than current price", 'error');
            }
        };
        this.toAuction = function (id) {
            console.log("in fucn");
            var lot = this.getLotById(id);
            lot.auction = true;
            lot.price = lot.start_price;

            this.sendToServer(
                {
                    auction: true,
                    id: lot.id,
                    price: 0
                },
                '/lot/update',
                'post',
                function () {
                    notification.create("Lot success added to auction", 'success');
                },
                function (data) {
                    notification.create(data.responseText, 'error');
                }
            )

        };
        this.removeFromAuction = function (id) {
            var lot = this.getLotById(id);
            lot.auction = null;

            this.sendToServer(
                {
                    auction: null,
                    id: lot.id,
                    price: lot.price
                },
                '/lot/update',
                'post',
                function () {
                    notification.create("Lot success removed from auction", 'success');
                },
                function (data) {
                    notification.create(data.responseText, 'error');
                }
            )
        };
        this.sendToServer = function (data, url, method, success, fail) {
            $.ajax({
                url: url,
                method: method,
                dataType: 'json',
                //contentType: 'application/json; charset=utf-8',
                data: data,
                cache: false
            }).success(function (data) {
                console.log("in done");
                (typeof success == "function") ? success(data) : false;
            }).error(function (data) {
                (typeof fail == "function") ? fail(data) : false;
                console.log("error");
            });
        }
    };

    common.api = new Api();

})();

/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 04.01.16
 */

$(document).ready(function () {

    if (window.history && window.history.pushState) {
        //if click button back reload content
        //The 'popstate' event only works when you push something before
        $(window).on('popstate', function () {
            common.spa.renderPage();
        });
    }

    var $body = $('body');

    $body.on("submit", '.js-add-lot-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);

        common.api.addLot($this);
        $('.modal').modal('hide');
        common.spa.renderPage();
    });

    $body.on("submit", '.js-add-bid-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);

        common.api.addBid($this);
        $('.modal').modal('hide');
        common.spa.renderPage();
    });
    /**
     * SEARCH
     */

    $body.on("submit", '.js-form-search', function (e) {
        e.preventDefault();
        var $this = $(this);

        if ($this.serializeObject().token != "") {
            common.search.searchByToken($this.serializeObject().token);
            changeHashOnValue('!/search');
        }
        common.spa.renderPage();
    });


    /**
     * ACTIONS WITH LOT
     */

    $body.on('click', '.js-to-auction', function (e) {
        var $this = $(this);
        common.api.toAuction($this.data('id'));
        common.spa.renderPage();
    });

    $body.on('click', '.js-remove-from-auction', function (e) {
        var $this = $(this);
        common.api.removeFromAuction($this.data('id'));
        common.spa.renderPage();
    });

    $body.on('click', '.js-add-lot', function (e) {
        var $modalAddLot = $('.js-modal-add-lot');
        $modalAddLot.modal('toggle');
    })

    $body.on('change', '.js-sort-lot', function (e) {
        common.lotsFilOrSort = lotsSortBy(this.value, common.lotsFilOrSort);
        common.spa.renderLots(common.lotsFilOrSort);
        //$('.js-sort-lot').val(this.value);
    });

    /**
     * FILTERS
     */

    $body.on('click', '.js-title-filter', function () {
        var $this = $(this);
        $this.toggleClass('off')
            .closest('.js-filter').find('.js-filter-inner')
            .animate({height: 'toggle'}, 350);
    });

    $body.on('submit', '.js-form-filters', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);

        var inputData = $this.serializeObject();

        common.lotsFilOrSort = common.filters.filterByPrice(common.lotsOnPage,
            parseInt(inputData.priceFrom),
            parseInt(inputData.priceTo));

        common.spa.renderLots(common.lotsFilOrSort);
    });

    $body.on('click', '.js-clear-filters', function () {
        common.filters.clearAllFilter();
    });

    /**
     * FOOTER
     */
    $body.on('click', '.js-open-contact', function () {
        var $mapContacts = $('.map-contacts');
        if ($mapContacts.hasClass('min-version'))
            $mapContacts.removeClass('min-version');
    });
    $body.on('click', '.js-close', function (e) {
        console.log("in cloxe");
        e.stopPropagation();
        var $mapContacts = $('.map-contacts');
        if (!$mapContacts.hasClass('min-version'))
            $mapContacts.addClass('min-version');
    });
    /**
     * PAGINATION
     */
    $body.on('click', '.js-on-pagination', function (e) {
        var $this = $(this);
        $this.closest('.js-pagination').find('.tool-pagination').removeClass('active');
        $this.addClass('active');
        common.pagination.onPageByNumber($this.data('number'));
    });

    /**
     * MENU
     */
    $body.on('click', '.js-toggle-menu', function (e) {
        var $this = $(this);
        $this.toggleClass('off')
            .closest('.js-wr-menu').find('.js-menu')
            .animate({height: 'toggle'}, 350);
    });
});
/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 10.01.16
 */

(function () {

    function Filters() {
        this.filterByPrice = function (lots, priceFrom, priceTo) {
            var flag;
            var flag1;
            if (!priceTo) flag = true;
            if (!priceFrom) flag1 = true;
            var resultArr = [];
            lots.map(function (elem) {
                if ((elem.start_price >= priceFrom || flag1 )
                    && (elem.start_price <= priceTo || flag))
                    resultArr.push(elem);
            });


            return resultArr;
        };
        this.clearAllFilter = function () {
            //common.lotsOnPage = lotsSortBy('cheap', common.api.get());
            //common.spa.renderLots();
            common.spa.renderPage();
        }
    };

    common.filters = new Filters();

})();

/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 10.01.16
 */

(function () {
    function initialize() {
        var position = {lat: 50.430452, lng: 30.487239};
        var mapCanvas = document.getElementById('map');
        var mapOptions = {
            center: new google.maps.LatLng(position.lat - 0.003, position.lng),
            zoom: 15,
            disableDefaultUI: true,
            zoomControl: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
        };
        var map = new google.maps.Map(mapCanvas, mapOptions)
        var marker = new google.maps.Marker({
            position: position,
            map: map,
            title: 'iLots'
        });
        var url = window.location.hostname;
    }

    google.maps.event.addDomListener(window, 'load', initialize);
})();


/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 22.05.2015
 */


/**
 *
 * TEMPLATES COMPILING
 */

function initTemplates() {

    var $templates = $('.js-templates');

    Array.prototype.map.call($templates, function (elem) {
        var source = $(elem).html();
        common.templates[elem.id] = Handlebars.compile(source);
    });


}

$(document).ready(function () {


    initTemplates();

    common.$content = $('.js-content');

    common.spa.renderPage();

});
/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 20.12.15
 */

Object.size = function (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

Object.min = function (obj) {
    var keys = Object.keys(obj);
    keys.map(function (element, i) {
        keys[i] = parseInt(element);
    });
    return Math.min.apply(null, keys);
};

notification = {
    notifications: {},
    counter: 0,
    create: function (text, type, seconds) {
        if (Object.size(this.notifications) > 4) {
            this.remove(Object.min(this.notifications));
        }

        var newNotification = {};
        newNotification.node = document.createElement("div");
        newNotification.node.innerHTML = text || "notification";
        newNotification.node.className = "notification " + (type || "info");

        newNotification.timeOut = setTimeout(function () {
            notification.remove(newNotification.id);
        }, seconds || 3000);


        newNotification.node.addEventListener('click', function () {
            notification.remove(newNotification.id);
        });
        var wrNotification = document.getElementById("wr-notification");
        if (!wrNotification){
            var newNode = document.createElement("div");
            newNode.setAttribute('id','wr-notification');
            newNode.setAttribute('class','wr-notification');
            document.getElementsByTagName('body')[0].appendChild(newNode);
            wrNotification = newNode;
        }
        wrNotification.appendChild(newNotification.node);
        newNotification.id = this.counter;
        this.notifications[this.counter] = newNotification;
        this.counter++;

    },
    remove: function (id) {
        if (!notification.notifications[id]) return; //для случая когда было нажато и закончился таймаут
        var node = notification.notifications[id].node;
        clearTimeout(notification.notifications[id].timeOut);
        this.animation(node,function(){
            node.parentNode.removeChild(node);
        });
        delete notification.notifications[id];
    },
    animation: function(node,callback){
        node.style.opacity = 1;
        var interval = setInterval(function(){
            var opacity = node.style.opacity - 0.01;
            node.style.opacity = opacity;
            if (opacity < 0) {
                clearInterval(interval);
                typeof callback == "function" ? callback() : false
            }
        },20);
    }
};




/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 10.01.16
 */


(function () {
    function Pagination() {
        this.lotsForPages = [];
        this.splitInputArr = function () {
            //TODO переделать через сплит
            var j = 0;
            var inputArr = common.lotsFilOrSort;
            this.lotsForPages[j] = [];
            this.lotsForPages[j].push(inputArr[0]);

            for (var i = 1; i < inputArr.length; i++) {
                if (i % 10 == 0) {
                    j++;
                    this.lotsForPages[j] = [];
                }
                this.lotsForPages[j].push(inputArr[i]);
            }
        };
        this.init = function () {
            this.lotsForPages = [];
            if (!common.lotsFilOrSort ||common.lotsFilOrSort.length <= 11)
                return false;
            this.splitInputArr();

            this.addButton();
            this.onPageByNumber(0);
        };
        this.onPageByNumber = function (number) {
            common.lotsOnPage = clone(this.lotsForPages[number]);
            common.lotsFilOrSort = clone(this.lotsForPages[number]);
            common.spa.renderLots(common.lotsFilOrSort);
        };
        this.addButton = function () {
            var $container = $('.js-pagination');
            var data = [];
            for (var i = 0; i < this.lotsForPages.length; i++) {
                data.push({name: i + 1, number: i});
            }
            $container.append(common.templates["js-template-pagination"]({data: data}));
            $container.find('.tool-pagination').first().addClass('active');
        }
    };

    common.pagination = new Pagination();

})();
/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 08.01.16
 */

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function changeHashOnValue(value) {
    window.location.hash = value;
};

var sortLots = {
    sortByPriceCheap: function (lots) {

        var arrTmp = clone(lots);

        function compare(a, b) {
            if (a.start_price < b.start_price)
                return -1;
            else if (a.start_price > b.start_price)
                return 1;
            else
                return 0;
        }

        arrTmp.sort(compare);
        return arrTmp;
    },
    sortByPriceExpansive: function (lots) {

        var arrTmp = clone(lots);

        function compare(a, b) {
            if (a.start_price > b.start_price)
                return -1;
            else if (a.start_price < b.start_price)
                return 1;
            else
                return 0;
        }

        arrTmp.sort(compare);
        return arrTmp;
    },
    sortByDateEarly: function (lots) {

        var arrTmp = clone(lots);

        arrTmp.sort(function (a, b) {
            return new Date(b.date_create_lot) - new Date(a.date_create_lot);
        });
        return arrTmp;
    },
    sortByDateLately: function (lots) {

        var arrTmp = clone(lots);

        arrTmp.sort(function (b, a) {
            return new Date(b.date_create_lot) - new Date(a.date_create_lot);
        });
        return arrTmp;
    }
}

function lotsSortBy(token,lots) {
    var sortedLots;
    switch (token) {
        case 'cheap':
            sortedLots = sortLots.sortByPriceCheap(lots);
            break;
        case 'expansive':
            sortedLots = sortLots.sortByPriceExpansive(lots);
            break;
        case 'early':
            sortedLots = sortLots.sortByDateEarly(lots);
            break;
        case 'lately':
            sortedLots = sortLots.sortByDateLately(lots);
            break;
    }
    return sortedLots;
}

/**
 *
 * ADDITIONAL FUNCTION
 */

function clone(obj) {
    if (obj == null || typeof(obj) != 'object')
        return obj;
    var temp = new obj.constructor();
    for (var key in obj)
        temp[key] = clone(obj[key]);
    return temp;
}

function changeFormatDate(date) {
    if (!date) return;
    var resultArr = date.match(/(\d\d\d\d-\d\d-\d\d)T(\d\d:\d\d)/);
    if (resultArr)
        return (resultArr[1] + " " + resultArr[2]).split('-').join('.');
    else return date;
}

function combineFormatDate(data) {
    data.date_create_lot = changeFormatDate(data.date_create_lot);
    data.date_last_bid = changeFormatDate(data.date_last_bid);
};

/**
 * SEARCH
 */

(function () {

    function Search() {
        this.resultSearch = [];
        this.searchByToken = function(token){
            var resultArray = [];
            common.api.lots.map(function (elem) {
                console.log("i");
                if(elem.title.indexOf(token) + 1) {
                    resultArray.push(elem);
                }
            });
            this.resultSearch = resultArray;
            console.log(resultArray);
        };
        this.getResultSearch = function () {
            return this.resultSearch;
        }
    };

    common.search = new Search();

})();

/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 09.01.16
 */

(function () {

    function Spa() {
        this.hash = {};
        this.hash.get = function () {
            var hashTmp = window.location.hash.substr(3);
            var arrTmp = [];
            return arrTmp.concat(hashTmp, hashTmp.split('/'));
        };


        this.renderPage = function () {
            var hash = this.hash.get();
            //hash[0] - all, hash[1] - page, hash[2] - param

            if (!hash[1]) {
                hash[1] = "main";
            }

            var template = common.templates["js-template-page-" + hash[1]];
            var templateWithData = {};

            if (template) {
                templateWithData = this.getTemplateWithData(template, hash);
                changeHashOnValue("#!/" + hash[0]);
            }
            else {
                templateWithData = common.templates["js-template-page-not-found"];
            }
            common.$content.empty().append(templateWithData);

            common.pagination.init();
        };
        this.renderLots = function (lots) {
            var data = lots || common.lotsOnPage;
            var valueSort = $('.js-sort-lot').val();
            data = lotsSortBy(valueSort, data);

            var $wrLots = $('.js-wr-lots');
            $wrLots.empty().append(common.templates["js-template-lots"]({lots: data}));
        };
        this.getTemplateWithData = function (template, hash) {
            var data = {};
            switch (hash[1]) {
                case 'lots':
                    var title = common.title.lots;
                    var lots;

                    if (/^lots\/\w*$/.test(hash[0])) { //if lots with parameter
                        lots = common.api.getLotsFilterByProperty(hash[2]);
                        if (hash[2] == 'discount')
                            title = common.title.discount;

                    }
                    else {
                        lots = common.api.get();
                    }
                    data.title = title;
                    //сортируем по дате
                    data.lots = lotsSortBy('early', lots);
                    //два одинаковых объекта нужно при фильтрации
                    common.lotsOnPage = data.lots; //for sorting and filtering lots
                    common.lotsFilOrSort = clone(data.lots); //for sorting and filtering lots
                    break;
                case 'lot':
                    var lot = common.api.getLotById(hash[2]);
                    combineFormatDate(lot);
                    data = {lot: lot};
                    break;
                case 'search':
                    data = {lots: common.search.getResultSearch()};
                    console.log(data);
                    break;
                default:
                    data = {};
            }

            return template(data);
        };
    };

    common.spa = new Spa();

})();

