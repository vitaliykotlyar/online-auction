/**
 * User: Vitalik Kotliar
 * Email: 7vetaly7@ukr.net
 * Date: 22.05.2015
 */


/**
 *
 * TEMPLATES COMPILING
 */

function initTemplates() {

    var $templates = $('.js-templates');

    Array.prototype.map.call($templates, function (elem) {
        var source = $(elem).html();
        common.templates[elem.id] = Handlebars.compile(source);
    });


}

$(document).ready(function () {


    initTemplates();

    common.$content = $('.js-content');

    common.spa.renderPage();

});